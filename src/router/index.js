import VueRouter from 'vue-router'

// Logger service
import Logger from '#services/logger'

// Routes
import privateRoutes from './routes/private.js'
import publicRoutes from './routes/public.js'

// Middlewares
import AdminMiddleware from './middlewares/AdminMiddleware'
import AuthMiddleware from './middlewares/AuthMiddleware'
import EmailVerifiedMiddleware from './middlewares/EmailVerifiedMiddleware'

// Routes
const routes = [
  ...privateRoutes,
  ...publicRoutes
]

// Router
const router = new VueRouter({
  routes,
  // To return to page top when navigate
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return {
        x: 0,
        y: 0,
        behavior: 'smooth'
      }
    }
  }
})

/* Global beforeEach actions */

router.beforeEach((to, from, next) => {
  // Log navigation
  Logger.consoleLog('router', `Navigate from "${from.fullPath}" to "${to.fullPath}"`)
  // Log layout loading
  if (to.meta.layout) {
    Logger.consoleLog('system', `Prepare to load layout "${to.meta.layout.name}" for the route "${to.fullPath}"`)
  }
  next()
})

/* Middlewares */

router.beforeEach(AuthMiddleware) // FIRST ! (Important) Auth middleware
router.beforeEach(AdminMiddleware) // Admin middleware
router.beforeEach(EmailVerifiedMiddleware) // Email verified middleware

export default router
