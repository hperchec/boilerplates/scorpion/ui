// Logger service
import Logger from '#services/logger'
// Store
import Store from '@/store'
// Utils
import { isDef } from '#utils'

/**
 * vue-router 'Email verified' middleware
 */
export default async (to, from, next) => {
  const requiresVerifiedEmail = isDef(to.meta.requiresVerifiedEmail) ? to.meta.requiresVerifiedEmail : false // False by default
  if (requiresVerifiedEmail) {
    Logger.consoleLog('router', `[EmailVerifiedMiddleware] Route "${to.fullPath}" requires email confirmation`)
    // Check if user has emailVerifiedAt value
    if (!Store.state.Auth.currentUser.emailVerifiedAt) {
      Logger.consoleLog('router', '[EmailVerifiedMiddleware] User email is not verified')
      Store.commit('SET_SYSTEM_SHOW_REQUIRES_VERIFIED_EMAIL_MODAL', true)
    }
  }
  next() // make sure to always call next()!
}
