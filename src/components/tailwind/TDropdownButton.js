// Vue-tailwind component
import { TButton } from 'vue-tailwind/dist/components'

export default {
  component: TButton,
  props: {
    fixedClasses: 'transition duration-100 ease-in-out active:shadow-inner border border-transparent',
    classes: 'w-full',
    variants: {
      primary: 'text-white bg-primary hover:bg-primary-300',
      secondary: 'text-white bg-secondary hover:bg-secondary-300',
      tertiary: 'bg-tertiary hover:bg-tertiary-600',
      error: 'text-white bg-error hover:bg-error-700',
      success: 'text-white bg-green-500 hover:bg-green-600',
      transparent: 'bg-transparent hover:bg-transparent'
    }
  }
}
