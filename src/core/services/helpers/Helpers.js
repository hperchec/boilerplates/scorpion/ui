/**
 * @vuepress
 * ---
 * title: Helpers class
 * headline: "App Helpers class"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

// Classes
import ServiceAsPlugin from '../ServiceAsPlugin.js'
// Dependencies
import camelize from 'lodash.camelcase'
import capitalize from 'lodash.capitalize'
import kebabcase from 'lodash.kebabcase'
import snakecase from 'lodash.snakecase'
import uppercase from 'lodash.uppercase'
import lowercase from 'lodash.lowercase'
import dateFormat from 'dateformat'

/**
 * Helpers class
 * @typicalname helpers
 * @extends ServiceAsPlugin
 * @classdesc
 * App helpers: Helpers class.
 * See also [ServiceAsPlugin]{@link ../ServiceAsPlugin}
 * ```javascript
 * // Import
 * import Helpers from '#services/helpers/Helpers'
 * ```
 */
class Helpers extends ServiceAsPlugin {
  /**
   * Create a new instance
   */
  constructor () { // eslint-disable-line no-useless-constructor
    // Call ServiceAsPlugin constructor
    super()
  }

  /**
   * camelize
   * @description Camelize a string (lodash.camelize).
   * Defined as Vue filter.
   * @example
   * Helpers.camelize('Foo Bar') // => 'fooBar'
   * {{ 'foo' | camelize }} // Vue filter
   * this.$h.camelize('Foo Bar') // => 'fooBar'
   * @param {string} str - The string to camelize
   * @return {string}
   */
  static camelize (str) {
    return camelize(str)
  }

  /**
   * capitalize
   * @description Capitalize the first letter of a string (lodash.capitalize).
   * Defined as Vue filter.
   * @example
   * Helpers.capitalize('Foo Bar') // => 'Foo bar'
   * {{ 'foo' | capitalize }} // Vue filter
   * @param {string} str - The string to capitalize
   * @return {string}
   */
  static capitalize (str) {
    return capitalize(str)
  }

  /**
   * snakecase
   * @description Transform a full string to snakecase (lodash.snakecase).
   * Defined as Vue filter.
   * @example
   * Helpers.snakecase('Foo Bar') // => 'foo_bar'
   * {{ 'foo' | snakecase }} // Vue filter
   * @param {string} str - The string to snakecase
   * @return {string}
   */
  static snakecase (str) {
    return snakecase(str)
  }

  /**
   * uppercase
   * @description Transform a full string to uppercases (lodash.uppercase).
   * Defined as Vue filter.
   * @example
   * Helpers.uppercase('Foo Bar') // => 'FOO BAR'
   * {{ 'foo' | uppercase }} // Vue filter
   * @param {string} str - The string to uppercase
   * @return {string}
   */
  static uppercase (str) {
    return uppercase(str)
  }

  /**
   * lowercase
   * @description Transform a full string to lowercases (lodash.lowercase).
   * Defined as Vue filter.
   * @example
   * Helpers.lowercase('Foo Bar') // => 'FOO BAR'
   * {{ 'foo' | lowercase }} // Vue filter
   * @param {string} str - The string to lowercase
   * @return {string}
   */
  static lowercase (str) {
    return lowercase(str)
  }

  /**
   * kebabcase
   * @description Transform a full string to kebabcase (lodash.kebabcase).
   * Defined as Vue filter.
   * @example
   * Helpers.kebabcase('Foo Bar') // => 'foo-bar'
   * {{ 'foo' | kebabcase }} // Vue filter
   * @param {string} str - The string to kebabcase
   * @return {string}
   */
  static kebabcase (str) {
    return kebabcase(str)
  }

  /**
   * dateFormat
   * @description Format Date (dateformat package)
   * @example
   * Helpers.dateFormat(new Date(), 'yyyy-mm-dd') // => '1900-01-01'
   * @param {Date} date - The date to format
   * @param {string} format - The  wanted format
   * @return {string}
   */
  static dateFormat (date, format) {
    return dateFormat(date, format)
  }

  /**
   * Install method for Vue
   * The service used as a plugin must absolutely overwrites this static method
   * @ignore
   * @param {Vue} _Vue - The Vue instance
   * @param {Object} [options={}] - The options of the plugin
   */
  static install (_Vue, options = {}) {
    const Vue = _Vue
    // Declare filters
    Vue.filter('camelize', Helpers.camelize)
    Vue.filter('capitalize', Helpers.capitalize)
    Vue.filter('kebabcase', Helpers.kebabcase)
    Vue.filter('snakecase', Helpers.snakecase)
    Vue.filter('uppercase', Helpers.uppercase)
    Vue.filter('lowercase', Helpers.lowercase)
    Vue.filter('dateFormat', Helpers.dateFormat)
    // Define '$h' for each Vue instance
    Vue.prototype.$h = Helpers
  }
}

export default Helpers
