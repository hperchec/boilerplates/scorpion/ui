// Config
import CONFIG from '@/config/config.js'
// Helpers
import Helpers from '#services/helpers'
// utils
import { isDef } from '#utils'
// Classes
import ServiceAsPlugin from '#services/ServiceAsPlugin.js'
/**
 * Logger class
 */
class Logger extends ServiceAsPlugin {
  /**
   * Create a new instance
   */
  constructor () { // eslint-disable-line no-useless-constructor
    // Call ServiceAsPlugin constructor
    super()
  }

  /**
   * consoleLog
   * @category methods
   * @description Log a message in the console
   * @param {string} type - Type ('info', 'advice', ...)
   * @param {string} message - The message
   * @param {Object} [options] - The options
   * @param {boolean} [options.dev] - Define if log must be displayed in development mode
   * @return {void}
   */
  consoleLog (type, message, options = { dev: true, prod: true }) {
    options = {
      dev: isDef(options.dev) ? options.dev : true,
      prod: isDef(options.prod) ? options.prod : true
    }
    let visibility = false
    // Development
    // !! VALUE OF CONFIG.DEVELOPMENT.SYSTEM.LOG is in top high level
    if (process.env.NODE_ENV === 'development' && CONFIG.DEVELOPMENT.SYSTEM.LOG && options.dev) {
      visibility = true
    }
    // Production
    // !! VALUE OF CONFIG.SYSTEM.LOG is in top high level
    if (process.env.NODE_ENV === 'production' && CONFIG.SYSTEM.LOG && options.prod) {
      visibility = true
    }
    // If log must be displayed
    if (visibility) {
      // Get log type configuration
      const messageBadge = CONFIG.SYSTEM.LOG_TYPES[type].messageBadge || CONFIG.SYSTEM.LOG_TYPES.default.messageBadge
      const messageBadgeColor = CONFIG.SYSTEM.LOG_TYPES[type].messageBadgeColor || CONFIG.SYSTEM.LOG_TYPES.default.messageBadgeColor
      const messageColor = CONFIG.SYSTEM.LOG_TYPES[type].messageColor || CONFIG.SYSTEM.LOG_TYPES.default.messageColor
      // Get time
      const atTime = Helpers.dateFormat(new Date(), 'HH:MM:ss.l')
      // Console log
      console.log(
        // Full string
        `%c${__APP_NAME__}%c${messageBadge}%c[${atTime}]%c${message}`,
        // CSS for %c1
        'line-height: 22px; padding: 4px; background-color: #362F2D; color: white; border-radius: 4px 0 0 4px;',
        // CSS for %c2
        `line-height: 22px; padding: 4px 4px 4px 0; background-color: #362F2D; color: ${messageBadgeColor}; border-radius: 0 4px 4px 0;`,
        // CSS for %c3
        `line-height: 22px; padding: 4px; color: ${__THEME_PRIMARY_COLOR__};`,
        // CSS for %c3
        `line-height: 22px; padding: 4px; color: ${messageColor};`
      )
    }
  }

  /**
   * Install method for Vue
   * The service used as a plugin must absolutely overwrites this static method
   * @ignore
   * @param {Vue} _Vue - Vue
   * @param {Object} [options={}] - The options of the plugin
   */
  static install (_Vue, options = {}) {
    const Vue = _Vue
    // Define '$Logger' for each Vue instance
    Vue.prototype.$Logger = new Logger()
  }
}

export default Logger
