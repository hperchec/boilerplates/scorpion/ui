import Vuex from 'vuex'
// I18n
import I18n from '@/i18n/'
// Modules
import Auth from './modules/Auth'
import Users from './modules/Users'
// Plugins
import SystemPlugin from './plugins/SystemPlugin'

const Store = new Vuex.Store({
  /**
  * modules
  * @alias module:store.modules
  * @type {Object}
  * @protected
  * @description Vuex Store option: store modules
  */
  modules: {
    Auth,
    Users
  },
  /**
   * plugins
   * @alias module:store.plugins
   * @type {Object}
   * @protected
   * @description Vuex Store option: store plugins
   */
  plugins: [
    SystemPlugin
  ],
  /**
  * state
  * @alias module:store.state
  * @type {Object}
  * @readonly
  * @description Vuex Store option. See syntax for accessor.
  */
  state: {
    /**
    * system
    * @description System data
    * @type {Object}
    */
    system: {
      /**
       * showMainLoading
       * @description Determine if show main loading
       * @type {boolean}
       * @default false
       */
      showMainLoading: false,
      /**
       * errorMessage
       * @description The current error message
       * @type {string}
       * @default null
       */
      errorMessage: null,
      /**
       * modals
       * @description The current error message
       * @type {Object}
       */
      modals: {
        /**
         * showErrorModal
         * @description Determine if show 'error modal'
         * @type {boolean}
         * @default false
         */
        showErrorModal: false,
        /**
         * showRequiresVerifiedEmailModal
         * @description Define if show 'requires verified email modal'
         * @type {boolean}
         * @default false
         */
        showRequiresVerifiedEmailModal: false,
        /**
         * showEmailSentModal
         * @description Define if show 'email sent modal'
         * @type {boolean}
         * @default false
         */
        showEmailSentModal: false,
        /**
         * showPasswordGuardModal
         * @description Define if show 'password guard modal'
         * @type {boolean}
         * @default false
         */
        showPasswordGuardModal: false,
        /**
         * showSignUpSuccessModal
         * @description Define if show 'Sign-Up success modal'
         * @type {boolean}
         * @default false
         */
        showSignUpSuccessModal: false
      }
    }
  },
  /**
  * getters
  * @alias module:store.getters
  * @type {Object}
  * @readonly
  * @description Vuex Store option. See syntax for accessor.
  */
  getters: {
    // Nothing for the moment
  },
  /**
  * actions
  * @alias module:store.actions
  * @type {Object}
  * @protected
  * @description Vuex Store option. See syntax for accessor.
  */
  actions: {
    /**
     * throwSystemError
     * @description Throw error and displays error modal
     * @param {string} [message] - (Optional) Error message
     * @return {void}
     * @example
     * this.$store.dispatch('throwSystemError', 'Unknown error appears')
     */
    throwSystemError: function ({ commit }, message) {
      // Show error modal
      commit('SET_SYSTEM_SHOW_ERROR_MODAL', true)
      // Set error message
      commit('SET_SYSTEM_ERROR_MESSAGE', message || I18n.t('errors.e0000'))
    },
    /**
     * clearSystemError
     * @description Reset system error
     * @return {void}
     * @example
     * this.$store.dispatch('clearSystemError')
     */
    clearSystemError: function ({ commit }) {
      // Show error modal
      commit('SET_SYSTEM_SHOW_ERROR_MODAL', false)
    }
  },
  /**
  * mutations
  * @alias module:store.mutations
  * @type {Object}
  * @protected
  * @description Vuex Store option. See syntax for accessor.
  */
  mutations: {
    /**
     * SET_SYSTEM_SHOW_MAIN_LOADING
     * @description Mutate state.showMainLoading
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_SYSTEM_SHOW_MAIN_LOADING (state, value) {
      state.system.showMainLoading = value
    },
    /**
     * SET_SYSTEM_ERROR_MESSAGE
     * @description Mutate state.system.errorMessage
     * @param {string} value - Error message
     * @return {void}
     */
    SET_SYSTEM_ERROR_MESSAGE (state, value) {
      state.system.errorMessage = value
    },
    /**
     * SET_SYSTEM_SHOW_ERROR_MODAL
     * @description Mutate state.system.modals.showErrorModal
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_SYSTEM_SHOW_ERROR_MODAL (state, value) {
      state.system.modals.showErrorModal = value
    },
    /**
     * SET_SYSTEM_SHOW_REQUIRES_VERIFIED_EMAIL_MODAL
     * @description Mutate state.system.modals.showRequiresVerifiedEmailModal
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_SYSTEM_SHOW_REQUIRES_VERIFIED_EMAIL_MODAL (state, value) {
      state.system.modals.showRequiresVerifiedEmailModal = value
    },
    /**
     * SET_SYSTEM_SHOW_PASSWORD_GUARD_MODAL
     * @description Mutate state.system.modals.showPasswordGuardModal
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_SYSTEM_SHOW_PASSWORD_GUARD_MODAL (state, value) {
      state.system.modals.showPasswordGuardModal = value
    },
    /**
     * SET_SYSTEM_SHOW_EMAIL_SENT_MODAL
     * @description Mutate state.system.modals.showEmailSentModal
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_SYSTEM_SHOW_EMAIL_SENT_MODAL (state, value) {
      state.system.modals.showEmailSentModal = value
    }
  }
})

export default Store
