const childp = require('child_process')
const colors = require('colors')

module.exports = function () {
  // Start
  console.log('- Script: create-tag - started ▶'.cyan)
  console.log('-----------------------------\n'.cyan)

  const CURRENT_VERSION = require('../globals.config.json').VERSION.CURRENT

  console.log(`Create tag: "v${CURRENT_VERSION}"...\n`)
  childp.execSync(
    `git tag -a v${CURRENT_VERSION} -m "v${CURRENT_VERSION}"`,
    {stdio: 'inherit'}
  )

  console.log(`Push tag...\n`)
  childp.execSync(
    `git push origin --follow-tags`
  )

  // End
  console.log('----\n'.cyan)
}
