# App (UI)

[![app](https://img.shields.io/static/v1?label=&message=App&color=362F2D&logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACWklEQVQ4T4WSTUhUURTH753PN2MTRZAtAinKdiGt2ojbCAIrmEghU4i+aMApyU3ja2aYmaaNOBLRQl0UUUESgSESDhTkJiRKTFv0gVEkBco0737MPaf7RuerUu/q8e45v3v+//+hpOoszya2+Ti5TySZcS1u6qWHQ7z6/n/ftPSTzSd3OyUdIxz2EQaEcJyGnGrzHjHfrwcpAwqzybsoSftKM8wgUxOEkS5lFZp8wfjHtSAVwLtEBwoyYgOQawiDTuDwkwhsMIKxwQ0BOGVuLjjcP/TrXrSnYAgoVMrz1tlHTbOwIcAukK/i87p5r262ZRAbRBlmJYe2urOJb+uaWAS8iH1HhvV2sy0FGC5QrqaIBQe1nNO+y+nnf0PKHtgXIhvjutFT9KEkg5NG+lueQIlRtFTUIIG4lqRfWDllAI7frJNOKwcWXHJwyKyOn3crdwP/tbSFKNeHIpTjhMFkO81kFmsBk+ZOyelrz6G+evEgcpEwdZwKfOk+k4jYhez6lWW0IGBPRwV5Ytzqb60B5J6Z+z2KvEEBQe+x6KNqrcwMN6Kic9qLojc62mHfnYGuGoAcu9aC0pnVC/QVODb7TlWWJ2f27HBx+KwlfNE+7NEmX/UPD6ZrAPyp2UoFjK6aJwhXEe+F1I3SJFZPdwvmIUwENFPpPOAb6f9UAxCPI53a6aHiiAxQ74LwgGMX7a7knz8fmlachANDA5P/pCAemk0gCuOU43Y9ogCuEsaSP6kjE6Xi/LnQUf/tgdFqf2r2AO/1buU5R4oyOKnjcusktKmODiOenltrlf8Awt9jILDjUAQAAAAASUVORK5CYII=)](https://gitlab.com/hperchec/boilerplates/scorpion/root)
[![app-ui](https://img.shields.io/static/v1?labelColor=362F2D&label=ui&message=v1.0.0&color=362F2D&logo=data:image/ico;base64,AAABAAEAEBAAAAEAIABoBAAAFgAAACgAAAAQAAAAIAAAAAEAIAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAD///8I////ev///+n/////////////////////////6v///+r/////////////////////////6f///3r///8I////Sf////D/////////////////////////zv///zX///81////zv/////////////////////////w////Sf///2f////8/////////////////////////9D///82////Nv///9D//////////////////////////P///2f///9n/////P/////////r////4P///+D////g////zv///87////g////4P///+D////r//////////z///9n////Z/////z////+////bv///xv///8f////H////yD///8g////H////x////8b////bv////7////8////Z////2f////8/////v///1n///8A////AP///wD///8A////AP///wD///8A////AP///1n////+/////P///2f///9n/////P////7///9a////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP///wD///9a/////v////z///9n////Z/////z////+////Wv///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD///8A////Wv////7////8////Z////2f////8/////v///1r///8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA////AP///1r////+/////P///2f///9n/////P////7///9a////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP///wD///9a/////v////z///9n////Z/////z////+////Wv///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD///8A////Wv////7////8////Z////2f////8/////v///1r///8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA////AP///1r////+/////P///2f///9n/////P////7///9Z////AP///wD///8A////AP///wD///8A////AP///wD///9Z/////v////z///9n////Z/////3////+////bv///xv///8f////H////x////8f////H////x////8b////bv////7////9////Z////0n////w/////////+v////g////4P///+D////g////4P///+D////g////4P///+v/////////8P///0n///8I////ev///+n/////////////////////////////////////////////////////////6f///3r///8IAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB+AAAAfgAAAH4AAAB+AAAAfgAAAH4AAAAAAAAAAAAAAAAAAAAAAAAA==)](https://gitlab.com/hperchec/boilerplates/scorpion/ui)
[![Docker](https://shields.io/static/v1?logo=docker&logoColor=white&label=&labelColor=2496ED&message=Docker&color=2496ED)](https://docker.com/)
[![Vuejs](https://shields.io/static/v1?logo=vuedotjs&label=&labelColor=5B5B5B&message=Vue.js&color=5B5B5B)](https://vuejs.org/)
[![Webpack](https://shields.io/static/v1?logo=webpack&label=&labelColor=5B5B5B&message=Webpack&color=5B5B5B)](https://webpack.js.org/)
[![Cordova](https://shields.io/static/v1?logo=apachecordova&label=&labelColor=5B5B5B&message=Cordova&color=5B5B5B)](https://cordova.apache.org/)
[![Discord](https://img.shields.io/discord/972997305739395092?label=Discord&labelColor=5562EA&logo=discord&logoColor=white&color=2F3035)](https://discord.gg/AWTgVAVKaR)
[![pipeline-status](https://gitlab.com/hperchec/boilerplates/scorpion/ui/badges/main/pipeline.svg)](https://gitlab.com/hperchec/boilerplates/scorpion/ui/commits/main)

[![author](https://img.shields.io/static/v1?label=&message=Author:&color=black)]()
[![herve-perchec](http://herve-perchec.com/badge.svg)](http://herve-perchec.com/)

**Table of contents**:

[[_TOC_]]

## Requirements

- Node v14.16.1
- NPM 6.14.12
- Cordova 10.0.0
- @vue/cli 4.5.15

## Get started

> **IMPORTANT**: Please refer to the parent repository: [app](https://gitlab.com/hperchec/boilerplates/scorpion/root).

If you want to clone this repository only:

```bash
git clone https://gitlab.com/hperchec/boilerplates/scorpion/ui.git
```

## Install

First, install `@vue/cli` and `cordova`

```bash
# vue-cli
npm install -g @vue/cli
# cordova
npm install -g cordova
```

> **WARNING**: cordova needs some requirements too (see documentation: [https://cordova.apache.org/docs/en/10.x/guide/cli/index.html](https://cordova.apache.org/docs/en/10.x/guide/cli/index.html))

Install project dependencies:

```bash
npm install
```

Then, prepare Cordova:

```bash
# Add Cordova platforms
npm run build # to create 'www/' folder
cordova platform add ios
cordova platform add android
cordova platform add electron
```

## Directory structure

Let's have a look to the directory structure:

```
ui/
├─ .vscode/
├─ bin/
├─ build/
├─ dist/
├─ lib/
├─ node_modules/
├─ public/
├─ readme/                   # For readme-generator
├─ src/
│  ├─ assets/                # All app assets
│  ├─ components/
│  ├─ config/                # See below for configuration
│  ├─ core/
│  ├─ i18n/
│  ├─ mixins/
│  ├─ models/
│  ├─ router/
│  ├─ scss/
│  │  ├─ _variables.scss_    # Variables file
│  │  └─ main.scss           # Main style entry
│  ├─ store/
│  │  ├─ modules/            # vuex store modules
│  │  ├─ plugins/            # vuex store plugins
│  │  └─ index.js
│  ├─ views/
│  └─ main.js
├─ tests/
├─ .eslintrc.js              # ESLint configuration
├─ .stylelintrc.json         # Stylelint configuration
├─ globals.config.json       # App globals definition
├─ jest.config.js            # jest (tests) configuration
├─ vue.config.js             # vue (+ webpack) configuration
└─ ...

```

## Development

```
# Start server
npm run dev
# or
npm run start
# or
npm run serve
```

## Production

```
# Build
npm run build
```

## Tests

```
# Unit
npm run test:unit
# End-to-end
npm run test:e2e
```

## Lints and fixes files

> **NOTE**: In development (via `npm run dev`), ESLint is automatically triggered at each file update by *webpack-dev-server*.

```
npm run lint
```

## Release

> **NOTE**: Local repository must be clean

- 1. Update CHANGELOG (see also [documentation](https://keepachangelog.com/en/1.0.0/))
- 2. New release (run command below)

```bash
# New release
npm run release
```

## Command list

> See also package.json file

```bash
npm run build # Build
npm run prebuild # Do nothing
npm run postbuild # Do nothing
npm run dev # npm run serve
npm run docs:generate # Generate docs (readme)
npm run info:version # Print current mystation-ui version
npm run version:new # Increments version (in globals.config.json & package.json) + git commit
npm run version:tag # Create new git tag
npm run release # Create new release
npm run serve # vue cli serve (development)
npm run start # vue cli serve (development)
npm run readme # Generate readme based on template
npm run test:unit # Start unit tests
npm run test:e2e # Start e2e tests
npm run lint # Lint code
```

## Globals

Let's have a look to the `globals.config.json` file:

```
ui/
├─ ...
├─ globals.config.json
└─ ...
```

```javascript
{
  "APP_NAME": "App",
  "VERSION": {
    "CURRENT": "1.0.0"
  },
  "SOCIAL": {
    "DISCORD_SERVER_ID": "972997305739395092",
    "DISCORD_INVITE_LINK": "https://discord.gg/AWTgVAVKaR"
  },
  "THEME": {
    "__THEME_PRIMARY_COLOR__":     "#F65D6C",
    "__THEME_SECONDARY_COLOR__":   "#F9A17C",
    "__THEME_TERTIARY_COLOR__":    "#FCDF8A",
    "__THEME_SUCCESS_COLOR__":     "#5DF57F",
    "__THEME_INFO_COLOR__":        "#F4EDFF",
    "__THEME_WARNING_COLOR__":     "#F5D776",
    "__THEME_ERROR_COLOR__":       "#FF0032",
    "__THEME_LIGHT_COLOR__":       "#FAFAFA",
    "__THEME_DARK_COLOR__":        "#362F2D",
    "__THEME_DARK_2_COLOR__":      "#4D423C",
    "__THEME_BREAKPOINTS_2XL__":   1536,
    "__THEME_BREAKPOINTS_XL__":    1280,
    "__THEME_BREAKPOINTS_LG__":    1024,
    "__THEME_BREAKPOINTS_MD__":    768,
    "__THEME_BREAKPOINTS_SM__":    640
  }
}
```

Every global defined in this file can be accessed:
- .js files
  ```javascript
  // Example: theme global
  const color = __THEME_PRIMARY_COLOR__
  ```
- .vue files
  ```vue
  <!-- Example: theme global in <template> -->
  <p :style="color: `${GLOBALS.__THEME_PRIMARY_COLOR__};`">Text</p>
  ```
  ```js
  // Example: theme global in <script> tag of the component
  this.color = this.GLOBALS.__THEME_PRIMARY_COLOR__
  ```
- .scss files
  ```scss
  p {
    color: $__THEME_PRIMARY_COLOR__;
  }
  ```

## Configuration

```
ui
├─ ...
├─ src
│  ├─ config
│  │  ├─ config.js
└─ ...
```

### Options

```javascript
// Color
const Color = require('color')
// snakecase
const snakecase = require('lodash.snakecase')

module.exports = {
  /**
   * System
   */
  SYSTEM: {
    /**
     * Define if logger verbose in production mode (must be true or false)
     * @default false
     * @type {boolean}
     */
    LOG: true,
    /**
     * Types configuration for Logger
     * @type {Object}
     */
    LOG_TYPES: {
      info: {
        messageBadge: '[INFO]',
        messageBadgeColor: Color('blue').lighten(0.4).hex(),
        messageColor: __THEME_PRIMARY_COLOR__
      },
      system: {
        messageBadge: '[SYSTEM]',
        messageBadgeColor: '#D6B38B',
        messageColor: '#AB8F6F'
      },
      router: {
        messageBadge: '[ROUTER]',
        messageBadgeColor: Color('purple').lighten(0.8).hex(),
        messageColor: 'purple'
      },
      advice: {
        messageBadge: '[ADVICE]',
        messageBadgeColor: '#122299',
        messageColor: __THEME_PRIMARY_COLOR__
      },
      warning: {
        messageBadge: '[WARN]',
        messageBadgeColor: __THEME_WARNING_COLOR__,
        messageColor: Color(__THEME_WARNING_COLOR__).darken(0.5).hex()
      },
      error: {
        messageBadge: '[ERROR]',
        messageBadgeColor: __THEME_ERROR_COLOR__,
        messageColor: __THEME_ERROR_COLOR__
      },
      default: {
        messageBadge: '[LOG]',
        messageBadgeColor: '#FFFF00',
        messageColor: __THEME_PRIMARY_COLOR__
      }
    },
    /**
     * Default theme
     * @type {string}
     */
    DEFAULT_THEME: 'light' // 'light', 'dark' or 'auto'
  },
  /**
   * Internationalization
   */
  I18N: {
    /**
     * Must be 'fr' or 'en'
     * @default 'fr'
     * @type {string}
     */
    LOCALE: 'fr',
    /**
     * Must be 'fr' or 'en'
     * @default 'fr'
     * @type {string}
     */
    FALLBACK_LOCALE: 'fr'
  },
  /**
   * APIs
   */
  API: {
    /**
     * Server API as Axios Instance options
     * @type {Object}
     */
    ServerAPI: {
      baseURL: '/api',
      allowedMethods: ['GET', 'PUT', 'PATCH', 'POST', 'DELETE', 'OPTIONS'],
      headers: {
        'X-Requested-With': 'XMLHttpRequest'
      },
      errorResponseKey: snakecase(__APP_NAME__ + '_error_response')
    }
  },
  /**
   * Authentication
   */
  AUTH: {
    /**
     * oAuth access token identifier (in browser LocalStorage)
     * @default 'app_access_token'
     * @type {string}
     */
    ACCESS_TOKEN_NAME: 'app_access_token',
    /**
     * oAuth refresh token identifier (in browser LocalStorage)
     * @default 'app_refresh_token'
     * @type {string}
     */
    REFRESH_TOKEN_NAME: 'app_refresh_token'
  },
  /**
   * Development options
   */
  DEVELOPMENT: {
    /**
     * System options in development mode
     */
    SYSTEM: {
      /**
       * Define if logger verbose in development mode (must be true or false)
       * @default true
       * @type {boolean}
       */
      LOG: true
    },
    /**
     * Define server API in development mode
     */
    API: {
      ServerAPI: {
        URL: 'http://localhost:8000', // Local server url
        APIBaseURL: 'http://localhost:8000/api'
      }
    }
  }
}

```

## Internal flow

```mermaid
%%{
    init: { 
        "theme": "base",
        "themeVariables": {
            "fontFamily": "Arial",
            "primaryTextColor": "#FAFAFA",
            "secondaryTextColor": "#FAFAFA",
            "tertiaryTextColor": "#FAFAFA",
            "lineColor": "#F65D6C",
            "primaryBorderColor": "#F65D6C",
            "secondaryBorderColor": "#F65D6C",
            "tertiaryBorderColor": "#F65D6C"
        }
    }
}%%
flowchart TB

    %% bootstrap.js
    subgraph bootstrap [ ]
        direction TB
        style bootstrap fill: #FEEEF0, stroke: none, stroke-width: 2px, rx: 4px, ry: 4px

        bootstrap_js("<div style='font-family:monospace'><span style='color:#D47BFA;'>import</span> <span style='color:#F65D6C'>bootstrap.js</span></div>")
        class bootstrap_js darkNode

        install("+ Plugins")
        class install primaryNode
        register("<div align='left'>+ Directives<br>+ Vendor components<br>+ Custom components")
        class register primaryNode
        load("+ Translations")
        class load primaryNode
        apply("+ Mixins")
        class apply primaryNode

        bootstrap_js -.- |"install"| install
        bootstrap_js -.- |"register"| register
        bootstrap_js -.- |"load"| load
        bootstrap_js -.- |"apply"| apply

    end

    %% Main function
    subgraph main [ ]
        direction TB
        style main fill: transparent, stroke-width: 2px, rx: 4px, ry: 4px

        %% boot method
        subgraph boot [ ]
            style boot fill: #FEEEF0, stroke: none, stroke-width: 2px, rx: 4px, ry: 4px

            boot_method("<div style='font-family:monospace'><span style='color:#D47BFA;'>await</span> <span style='color:#FCDF8A'>boot</span>()</div>")
            class boot_method darkNode

            check_auth("<div style='text-align:left'>Check if user is authenticated</div><div style='display: inline-block; vertical-align: top'>➜</div> <div style='text-align:left; display: inline-block; font-size:14px'>Based on the <span style='font-family:monospace; color:#FCDF8A'>access_token</span> <br>saved in <span style='font-family:monospace; color:purple;'>localStorage</span></div>")
            class check_auth primaryNode

            boot_method --- |"<i style='color:#362F2D !important;'>(async)</i>"| check_auth

        end
        
        %% Vue instance
        subgraph vue_instance [ ]
            direction TB
            style vue_instance fill: #FEEEF0, stroke: none, stroke-width: 2px, rx: 4px, ry: 4px

            %% new Vue()
            new_vue("<div style='font-family:monospace'><span style='color:#FCDF8A;'>new</span> <span style='color:#7AFAAF'>Vue</span>( fa:fa-list-ul )</div>")
            class new_vue darkNode


            %% window.__ROOT_INSTANCE__
            root_instance{{"<div style='font-family:monospace;'><span style='color:cyan;'>window</span>.<span style='color:cyan;'>__ROOT_INSTANCE__</span></div>"}}
            class root_instance darkNode

            %% Router
            router("<span style='color:#F65D6C;'>Router <span style='color:#362F2D'>(vue-router)</span></span>")
            class router invertedNode

            %% Root instance hooks
            subgraph hooks ["<i style='color:#362F2D !important; font-size:14px;'>Root instance hooks</i>"]
                direction LR
                style hooks fill: transparent, stroke-width: 0px, rx: 4px, ry: 4px

                before_create("<div style='text-align:left'><div style='font-family:monospace;'>beforeCreate()</div><div style='font-size:14px'>+ Init core modules</div></div>")
                class before_create primaryNode
                created("<div style='text-align:left'><div style='font-family:monospace;'>created()</div><div style='font-size:14px'>+ Set theme</div><div style='font-size:14px'>+ Detect device</div><div style='font-size:14px'>+ Window resize event listener</div></div>")
                class created primaryNode
                mounted("<div style='text-align:left'><div style='font-family:monospace;'>mounted()</div></div>")
                class mounted primaryNode

                before_create --> |"···"| created
                created --> |"···"| mounted

            end

            new_vue -.- |"<span style='color:#362F2D !important;'>assigned to</span>"| root_instance
            linkStyle 7 stroke:#362F2D
            new_vue --> |"triggers"| router
            router --> |"after routing"| hooks

        end

        boot --> |"when booted"| vue_instance

    end

    render_app("Render <b style='font-family:monospace; font-size:18px; color:#7AFAAF'>#60;App/&#62;</b> component")
    class render_app darkNode

    %% Top level links
    bootstrap --> |"<b style='font-family:monospace; font-size:17px'>main()</b>function&nbsp;"| main
    main --> |"Root instance mounted"| render_app

    %% Style definitions

    %% Overwrites link edges
    classDef edgeLabel color:#F65D6C, background-color:#FEEEF0, padding:6px, border-radius:10px;
    
    %% Custom classes
    classDef darkNode fill:#362F2D, stroke: none;
    classDef primaryNode fill:#F65D6C, stroke:none;
    classDef invertedNode fill:transparent, stroke-width:2;

```

## Dependencies

<details>
<summary><b>Global</b> (click to expand)</summary>

| name                         | version |
| ---------------------------- | ------- |
| @cospired/i18n-iso-languages | ^3.0.0  |
| core-js                      | ^3.6.5  |
| dateformat                   | ^4.5.1  |
| lodash.camelcase             | ^4.3.0  |
| lodash.capitalize            | ^4.2.1  |
| lodash.isequal               | ^4.5.0  |
| lodash.lowercase             | ^4.3.0  |
| lodash.maxby                 | ^4.6.0  |
| lodash.minby                 | ^4.6.0  |
| lodash.samplesize            | ^4.2.0  |
| lodash.snakecase             | ^4.1.1  |
| lodash.uppercase             | ^4.3.0  |
| mobile-device-detect         | ^0.4.3  |
| vue                          | ^2.6.11 |
| vue-awesome                  | ^4.3.1  |
| vue-i18n                     | ^8.24.4 |
| vue-ripple-directive         | ^2.0.1  |
| vue-router                   | ^3.2.0  |
| vue-tailwind                 | ^2.5.0  |
| vuex                         | ^3.4.0  |

</details>

<details>
<summary><b>Dev</b> (click to expand)</summary>

| name                            | version                                  |
| ------------------------------- | ---------------------------------------- |
| @hperchec/readme-generator      | ^1.0.4                                   |
| @intlify/eslint-plugin-vue-i18n | ^0.11.1                                  |
| @vue/cli-plugin-babel           | ~4.5.0                                   |
| @vue/cli-plugin-e2e-nightwatch  | ~4.5.0                                   |
| @vue/cli-plugin-eslint          | ~4.5.0                                   |
| @vue/cli-plugin-router          | ~4.5.0                                   |
| @vue/cli-plugin-unit-jest       | ~4.5.0                                   |
| @vue/cli-plugin-vuex            | ~4.5.0                                   |
| @vue/cli-service                | ~4.5.0                                   |
| @vue/eslint-config-standard     | ^5.1.2                                   |
| @vue/test-utils                 | ^1.0.3                                   |
| autoprefixer                    | ^9.8.6                                   |
| animate-sass                    | ^0.8.2                                   |
| babel-eslint                    | ^10.1.0                                  |
| chromedriver                    | ^84.0.1                                  |
| colors                          | ^1.4.0                                   |
| cordova-android                 | ^9.1.0                                   |
| cordova-browser                 | ^6.0.0                                   |
| cordova-electron                | ^1.1.1                                   |
| cordova-plugin-whitelist        | ^1.3.4                                   |
| eslint                          | ^6.7.2                                   |
| eslint-plugin-import            | ^2.20.2                                  |
| eslint-plugin-node              | ^11.1.0                                  |
| eslint-plugin-promise           | ^4.2.1                                   |
| eslint-plugin-standard          | ^4.0.0                                   |
| eslint-plugin-vue               | ^6.2.2                                   |
| git-status                      | ^1.0.10                                  |
| markdown-table                  | ^2.0.0                                   |
| node-sass                       | ^4.12.0                                  |
| postcss                         | ^7.0.36                                  |
| sass-loader                     | ^8.0.2                                   |
| stylelint                       | ^13.13.1                                 |
| stylelint-config-standard       | ^22.0.0                                  |
| stylelint-scss                  | ^3.19.0                                  |
| stylelint-webpack-plugin        | ^2.1.1                                   |
| tailwindcss                     | npm:@tailwindcss/postcss7-compat@^2.2.10 |
| vue-template-compiler           | ^2.6.11                                  |

</details>

## Docker

> **NOTE**: Remember that the docker images & containers are managed in the project top-level directory (/app): see [documentation](https://gitlab.com/hperchec/boilerplates/scorpion/root).

### Build the image

```bash
docker build -t node-14.19.1 .
```

> **NOTE**: `node-14.19.1` will be the name of the image. You can modify it as you want.

### Run the container

> **IMPORTANT**
>
> Using docker container in <u>development</u> for this subproject ('UI') is not optimized in Windows environment!
>
> Webpack-dev-server hot reload is extremely slow when it used without **cached volume** due to the size of the `node_modules` folder.
>
> Let's have a look to the `docker-compose.yml` file in the [parent repository](https://gitlab.com/hperchec/boilerplates/scorpion/root)! The *MODE* option for mounted volumes is only available for Linux or MacOS platform ([details](https://docs.docker.com/storage/bind-mounts/#configure-mount-consistency-for-macos)).
>
> Unfortunately, Windows does not support **cached volume**... Pray for Windows improvements in the future ¯\\_(ツ)_/¯
>
> There seems to be a workaround as explained in this [tutorial](https://www.youtube.com/watch?v=dQw4w9WgXcQ).
>
> More seriously, if you want to use Docker for this subproject without mounting the volume, comment the *volumes* section of the *ui* service in the `docker-compose.yml` file. Then, connect to the container via **SSH** once the container is up. You can use the Visual Studio Code extension [kelvin.vscode-sshfs](https://marketplace.visualstudio.com/items?itemName=Kelvin.vscode-sshfs) (as mentionned in the `.vscode/extensions.json` file) to connect to your container and to easily make your changes in your local machine.
>
> However, if you don't use Docker, you need to have NodeJS installed in your local machine. See [requirements](#requirements) section.

This command will run the PHP container named `ui` based on the `node-14.19.1` image previously created.

Note that the **container port** `8080` will be related to the **local port** `8080`.

```bash
docker run -d -p 8080:8080 --name ui node-14.19.1
```

### Using GitLab

Login to gitlab registry:

```bash
docker login registry.gitlab.com
```

Build and push image to gitlab registry:

```bash
# Build the image (using 'prod' stage, see Dockerfile)
docker build --target prod -t registry.gitlab.com/hperchec/boilerplates/scorpion/ui .
# Push the image to the container registry
docker push registry.gitlab.com/hperchec/boilerplates/scorpion/ui
```

----

Made with ❤ by [**Hervé Perchec**](http://herve-perchec.com/)